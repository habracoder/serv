<?php
/**
 * Вспомагательный класс-сущность, для конретизации и описания параметров хеша
 */
namespace AppBundle\Entity;

/**
 * Class Hash
 * @package Service
 */
class Hash
{
    /**
     * Show IP
     * @var string
     */
    private $ip;

    /**
     * Timestamp
     * @var int
     */
    private $time;

    /**
     * User-Agent Sum
     * @var int
     */
    private $uaCheckSum;

    /**
     * Informer ID
     * @var int
     */
    private $widget;

    /**
     * Teaser ID
     * @var int
     */
    private $teaser;

    /**
     * Partner ID
     * @var int
     */
    private $campaign;

    /**
     * Source ID
     * @var int
     */
    private $source;

    /**
     * POC
     * @var float
     */
    private $price;

    /**
     * Inf Page
     * @var int
     */
    private $page;

    /**
     * Position page
     * @var int
     */
    private $position;

    /**
     * Subnet ID
     * @var int
     */
    private $subnet;

    /**
     * DSP
     * @var int
     */
    private $rtbClick;

    /**
     * Data-centre ID
     * @var string
     */
    private $dcId;

    /**
     * Quality factor
     * @var int
     */
    private $qualityFactor;

    /**
     * Item_type
     * @var int
     */
    private $itemType;

    /**
     * Checksum
     * @var string
     */
    private $hashCheckSum;

    /**
     * Constructor
     * @param $data
     */
    public function __construct(array $data = [])
    {
        $this->ip = '0.0.0.0';
        $this->time = time();
        $this->price = 0;
        $this->page = 1;
        $this->position = 0;
        $this->subnet = 0;
        $this->qualityFactor = 100;
        $this->itemType = 0;

        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * @param null $default
     * @return null|string
     */
    public function getIp($default = null)
    {
        return null !== $this->ip ? $this->ip : $default;
    }

    /**
     * @param string $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @param int $default
     * @return int
     */
    public function getTime($default = null)
    {
        return null !== $this->time ? $this->time : $default;
    }

    /**
     * @param int $time
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @param $default
     * @return int
     */
    public function getUaCheckSum($default = null)
    {
        return null !== $this->uaCheckSum ? $this->uaCheckSum : $default;
    }

    /**
     * @param int $uaCheckSum
     * @return $this
     */
    public function setUaCheckSum($uaCheckSum)
    {
        $this->uaCheckSum = $uaCheckSum;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getWidget($default = null)
    {
        return null !== $this->widget ? $this->widget : $default;
    }

    /**
     * @param int $widget
     * @return $this
     */
    public function setWidget($widget)
    {
        $this->widget = $widget;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getTeaser($default = null)
    {
        return null !== $this->teaser ? $this->teaser : $default;
    }

    /**
     * @param int $teaser
     * @return $this
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getCampaign($default = null)
    {
        return null !== $this->campaign ? $this->campaign : $default;
    }

    /**
     * @param int $campaign
     * @return $this
     */
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getSource($default = null)
    {
        return null !== $this->source ? $this->source : $default;
    }

    /**
     * @param int $source
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @param null $default
     * @return float|null
     */
    public function getPrice($default = null)
    {
        return null !== $this->price ? $this->price : $default;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getPage($default = null)
    {
        return null !== $this->page ? $this->page : $default;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getPosition($default = null)
    {
        return null !== $this->position ? $this->position : $default;
    }

    /**
     * @param int $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getSubnet($default = null)
    {
        return null !== $this->subnet ? $this->subnet : $default;
    }

    /**
     * @param int $subnet
     * @return $this
     */
    public function setSubnet($subnet)
    {
        $this->subnet = $subnet;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getRtbClick($default = null)
    {
        return null !== $this->rtbClick ? $this->rtbClick : $default;
    }

    /**
     * @param int $rtbClick
     * @return $this
     */
    public function setRtbClick($rtbClick)
    {
        $this->rtbClick = $rtbClick;

        return $this;
    }

    /**
     * @param null $default
     * @return null|string
     */
    public function getDcId($default = null)
    {
        return null !== $this->dcId ? $this->dcId : $default;
    }

    /**
     * @param $dcId
     * @return $this
     */
    public function setDcId($dcId)
    {
        $this->dcId = $dcId;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getQualityFactor($default = null)
    {
        return null !== $this->qualityFactor ? $this->qualityFactor : $default;
    }

    /**
     * @param int $qualityFactor
     * @return $this
     */
    public function setQualityFactor($qualityFactor)
    {
        $this->qualityFactor = $qualityFactor;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getItemType($default = null)
    {
        return null !== $this->itemType ? $this->itemType : $default;
    }

    /**
     * @param int $itemType
     * @return $this
     */
    public function setItemType($itemType)
    {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * @param null $default
     * @return int|null
     */
    public function getHashCheckSum($default = null)
    {
        return null !== $this->hashCheckSum ? $this->hashCheckSum : $default;
    }

    /**
     * @param int $hashCheckSum
     * @return $this
     */
    public function setHashCheckSum($hashCheckSum)
    {
        $this->hashCheckSum = $hashCheckSum;

        return $this;
    }

    /**
     * Return data as array
     * @return array
     */
    public function toArray()
    {
        $array = [];

        foreach (get_object_vars($this) as $property => $value) {
            $array[$property] = $value;
        }

        return $array;
    }
}
