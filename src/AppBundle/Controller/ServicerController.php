<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ServicerController
 * @package AppBundle\Controller
 */
class ServicerController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     * @throws \Twig_Error
     */
    public function runAction(Request $request)
    {
        // Опредиление если от радатчика необходима картинка
        if (preg_match('/imgg\./', $request->getHost())) {
            return $this->forward('AppBundle:Servicer:image');
        }

        $servicer = $this->get('servicer');
        $template = "AppBundle::{$servicer->getType()}_pv{$servicer->getProtocolVersion()}.html.twig";
        $teasers = $servicer->getTeasers();

        return new Response(
            $this->get('templating')->render($template, [
                'teasers' => $teasers,
                'method' => $servicer->getMethod()
            ]),
            200,
            ['Content-Type' => 'application/x-javascript; charset=utf-8']
        );
    }

    /**
     * Генерация случайной картинки
     * @return Response
     */
    public function imageAction()
    {
        ob_start();
        $im = imagecreatefromjpeg($this->get('image')->getImage());
        imagejpeg($im);
        imagedestroy($im);

        $imageString = ob_get_contents();
        ob_clean();

        return new Response(
            $imageString,
            200,
            ['Content-Type' => 'image/jpeg']
        );
    }
}
