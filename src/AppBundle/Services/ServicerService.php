<?php

namespace AppBundle\Services;

use AppBundle\Entity\Hash;
use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class ServicerService
{
    /**
     * Constants
     */
    const TYPE_NEWS = 'news';
    const TYPE_GOODS = 'goods';
    const TYPE_COMPOSITE = 'composite';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Форсированый показ
     * @var bool
     */
    protected $fs = false;

    /**
     * g_blocks|teaser.id
     * @var
     */
    protected $tickerId;

    /**
     * g_blocks|teaser.uid
     * @var
     */
    protected $tickerUid;

    /**
     * Номер страницы
     * @var
     */
    protected $page;

    /**
     * Тип информера (news|goods)
     * @var
     */
    protected $type;

    /**
     * Версия протокола
     * @var int
     */
    protected $pv = 5;

    /**
     * Данные по изображению для ответа
     * @var \stdClass
     */
    protected $image = null;

    /**
     * Данные по виджету
     * @var \stdClass
     */
    protected $widget;

    /**
     * Набор параметров которые пришли GET запросом на раздатчик
     * @var \stdClass
     */
    protected $query;

    /**
     * @var bool
     */
    protected $isRedirector;

    /**
     * Constructor
     * @param ContainerInterface $container
     * @throws \Exception
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->parseParams();
        $this->initWidget();
        $this->checkRedirector();
        $this->checkDefaults();
    }

    /**
     * Разбор url строки запроса к раздатчику
     * @throws \Exception
     */
    private function parseParams()
    {
        $path = $this->container->get('request')->getPathInfo();

        preg_match('/(?<fs>\/fs|ns)?(?<tickerUid>\/u[0-9]+)(?<tickerId>\/[0-9]+)(?<page>\/[0-9]+)?/', $path, $match);

        if (!$match) {
            throw new \Exception('Cant\'t parse url string.');
        }

        // Опредиление основных параметров
        $this->fs           = (bool) trim($match['fs'], '/');
        $this->tickerUid    = (int) trim($match['tickerUid'], '/u');
        $this->tickerId     = (int) trim($match['tickerId'], '/');
        $this->page = isset($match['page']) && $match['page'] ? (int) trim($match['page'], '/') : 1;

        // Опредиление типа виджета
        preg_match('/[a-z]{2}\-(?<type>[a-z]{1})/', $_SERVER['HTTP_HOST'], $typeMatch);
        $this->type = $typeMatch['type'] == 'g' ? self::TYPE_GOODS : self::TYPE_NEWS;

        // Сохранение доп. параметров
        parse_str($_SERVER['QUERY_STRING'], $params);
        $this->query = (object) $params;

        return $this;
    }

    /**
     * Инициализация данных для виджета
     * @return $this
     * @throws \Doctrine\DBAL\DBALException
     */
    private function initWidget()
    {
        $table = $this->type == self::TYPE_GOODS ? 'g_blocks' : 'tickers';
        /** @var Connection $con */
        $con = $this->container->get('doctrine')->getConnection();

        if ($this->tickerUid) {
            $where = "uid = {$this->tickerUid}";
        } else {
            $where = "id = {$this->tickerId}";
        }

        $this->widget = $con->query("SELECT * FROM {$table} WHERE {$where}")->fetch(\PDO::FETCH_OBJ);

        $this->image = $con
            ->query("SELECT * FROM tickers_image_format WHERE id = {$this->widget->image_format}")
            ->fetch(\PDO::FETCH_OBJ);

        return $this;
    }

    /**
     * Проверка необходимо ли выдавать редиректор
     * @return void
     */
    private function checkRedirector()
    {
        /** @var Connection $con */
        $con = $this->container->get('doctrine')->getConnection();

        if ($this->widget->parent && $this->widget->class == 'composite') {
            $dayOfWeek = date('N') >= 6 ? 'weekday' : 'holyday';
            $res = $con
                ->query("
                    SELECT *
                    FROM tickers_composite_default
                    WHERE composite = '{$this->widget->parent}'
                    AND day_of_week = '{$dayOfWeek}'
                    ")
                ->fetch(\PDO::FETCH_OBJ);

            if ($res && $res->type != $this->type) {
                $this->isRedirector = true;
            }
        }
    }

    /**
     * Проверка дефолтных значений
     */
    private function checkDefaults()
    {
        if (isset($this->query->pv) && $this->query->pv) {
            $this->pv = (int) $this->query->pv;
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getProtocolVersion()
    {
        return $this->pv;
    }

    /**
     * @return int
     */
    private function getCountNews()
    {
        return (int) $this->widget->count_news;
    }

    /**
     * Сбор данных для ответа
     * @return array
     */
    public function getTeasers()
    {
        $textGenerator = $this
            ->container
            ->get('text.generator')
            ->setType($this->getType());

        $teasers = [];
        for ($i = 0; $i < $this->getCountNews(); $i++) {
            $price = rand(500, 5000);
            $teaserId = rand(100000, 999999);
            $hash = (new Hash())
                ->setPrice($price)
                ->setWidget($this->widget->id)
                ->setItemType($this->type)
                ->setSubnet($this->widget->subnet)
                ->setTeaser($teaserId);

            $teasers[] = [
                'id'    => $teaserId,
                'title' => $textGenerator->generateText(),
                'partnerName' => $textGenerator->getRandomPartnerName(),
                'price' => sprintf('%s грн.', number_format($price, 2)),
                'priceOld' => rand(0, 1) > 0 ? sprintf('%s грн.', number_format($price + ($price/2), 2))  : '',
                'image' => $this->getImageLink(),
                'hash' => $this->container->get('hash.generator')->encodeHash($hash)
            ];
        }

        return $teasers;
    }

    /**
     * Создание линка для картинки, с учетом размеров.
     * @return string
     */
    private function getImageLink()
    {
        return sprintf(
            'http://imgg.servicer.net/%s/%s_%sx%s.jpg',
            rand(1000, 9999),
            rand(1000000, 9999999),
            $this->image->width,
            $this->image->height
        );
    }

    /**
     * Подготовка метода который будет в ответе
     * @return string
     */
    public function getMethod()
    {
        /** @var Connection $con */
        $con = $this->container->get('doctrine')->getConnection();
        $informerPrefix = $con->query("
            SELECT informer_prefix
            FROM subnets
            WHERE id = {$this->widget->subnet}
        ")->fetch(\PDO::FETCH_COLUMN);

        $method = $informerPrefix;
        if ($this->isRedirector) {
            $method .= sprintf('RedirectComposite%s', $this->widget->parent);
        } else {
            $method .= sprintf('Load%s%s', ucfirst($this->type), $this->widget->id);
        }

        return $method;
    }
}