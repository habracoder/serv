<?php
/**
 * Сервис для удобности шифрования и расшифрования хеша для тестов
 */
namespace AppBundle\Services;
use AppBundle\Entity\Hash;

/**
 * Class HashService
 * @package Service
 */
class HashService
{
    /**
     * Расшифровка хеша
     * @param string
     * @return array
     */
    public function decodeHash($hash)
    {
        $b = pow(2, 8);
        $hashObj = new Hash();
        $s = base64_decode(strtr($hash, [
            '-' => '+',
            '_' => '/',
            '*' => '=',
        ]));

        $key = "1234567890123456";
        $s = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $s, MCRYPT_MODE_ECB);

        // опредиление ip
        $ip = ord($s[4]) + ord($s[5]) * $b + ord($s[6]) * pow($b, 2) + ord($s[7]) * pow($b, 3);
        $ip = long2ip($ip);
        $mIp = explode(".", $ip);
        $ip = $mIp[3] . "." . $mIp[2] . "." . $mIp[1] . "." . $mIp[0];

        $hashObj->setIp($ip)
            ->setTime(ord($s[0]) + ord($s[1]) * $b + ord($s[2]) * pow($b, 2) + ord($s[3]) * pow($b, 3))
            ->setUaCheckSum(ord($s[8]) + ord($s[9]) * $b + ord($s[10]) * pow($b, 2) + ord($s[11]) * pow($b, 3))
            ->setWidget(ord($s[12]) + ord($s[13]) * $b + ord($s[14]) * pow($b, 2) + ord($s[15]) * pow($b, 3))
            ->setTeaser(ord($s[16]) + ord($s[17]) * $b + ord($s[18]) * pow($b, 2) + ord($s[19]) * pow($b, 3))
            ->setCampaign(ord($s[20]) + ord($s[21]) * $b + ord($s[22]) * pow($b, 2) + ord($s[23]) * pow($b, 3))
            ->setSource(ord($s[24]) + ord($s[25]) * $b + ord($s[26]) * pow($b, 2) + ord($s[27]) * pow($b, 3))
            ->setPrice(ord($s[28]) + ord($s[29]) * $b + ord($s[30]) * pow($b, 2) + ord($s[31]) * pow($b, 3))
            ->setPage(ord($s[32]))
            ->setPosition(ord($s[33]))
            ->setSubnet($s[34])
            ->setRtbClick(ord($s[35]))
            ->setDcId($s[36])
            ->setQualityFactor((ord($s[37]) + ord($s[38]) * $b) / 100)
            ->setItemType(ord($s[39]));

        return $hashObj->toArray();
    }

    /**
     * Шифрование параметров
     * Encode show hash from get params
     * @param array|Hash $hashObj - Example: {
     *      time:           1434468679
     *      ip:             91.197.49.165
     *      uaCheckSum:     1365478910
     *      widget:         5264
     *      teaser:         5923249
     *      campaign:       102986
     *      source:         0
     *      price:          32
     *      page:           2
     *      position:       2
     *      rtbClick:       0
     *      dcId:           e
     *      qualityFactor:  100
     *      itemType:       0
     *      source:         0
     *      hashCheckSum:   933$b77
     *  }
     * @return string
     */
    public function encodeHash($hashObj)
    {
        // объект параметров
        if (!$hashObj instanceof Hash) {
            $hashObj = new Hash($hashObj);
        }

        // Замудренная шифровалка ИП адресса в обратную сторону
        $ipLong = intval(
            implode(
                '',
                unpack(
                    'N',
                    pack('C4', ...array_reverse(
                        explode('.', $hashObj->getIp('91.197.49.165'))
                    ))
                )
            )
        );

        // Упаковка данных у бинарную строку
        $binStr = pack('IIIIIIIICCCCAvCI', ...array_values([
            'time' => $hashObj->getTime(time()),
            'ipLong' => $ipLong,
            'uaCheckSum' => $hashObj->getUaCheckSum(),
            'widget' => $hashObj->getWidget(),
            'teaser' => $hashObj->getTeaser(),
            'campaign' => $hashObj->getCampaign(),
            'source' => $hashObj->getSource(0),
            'price' => $hashObj->getPrice(32),
            'page' => $hashObj->getPage(2),
            'position' => $hashObj->getPosition(3) - 1,
            'subnet' => $hashObj->getSubnet(),
            'rtbClick' => $hashObj->getRtbClick(),
            'dcId' => $hashObj->getDcId(),
            'qualityFactor' => $hashObj->getQualityFactor(),
            'itemType' => $hashObj->getItemType(),
            'hashCheckSum' => $hashObj->getHashCheckSum()
        ]));

        // Шифрование бинарной строки
        $hash = mcrypt_encrypt(
            MCRYPT_RIJNDAEL_128,
            '1234567890123456',
            $binStr,
            MCRYPT_MODE_ECB
        );

        // упаковка шифра в base64 строку
        $hash = base64_encode($hash);

        // замена символов не совместимых с уролом
        $hash = strtr($hash, [
            '+' => '-',
            '/' => '_',
            '=' => '*',
        ]);

        return $hash;
    }
}
