<?php

namespace AppBundle\Services;

use Symfony\Component\HttpKernel\Config\FileLocator;

/**
 * Class TextGeneratorService
 * @package AppBundle\Services
 */
class TextGeneratorService
{
    /**
     * @var FileLocator
     */
    protected $fileLocator;

    /**
     * Тип контента, товарный или новостной
     * @var
     */
    protected $type;

    /**
     * Сырые строчки, еще до обработки
     * @var array
     */
    protected $lines = [];

    /**
     * Строчки проинициализированные
     * @var bool
     */
    protected $initialized = false;

    /**
     * Использованные строки, дабы избежать дублированнности
     * @var array
     */
    protected $used = [];

    /**
     * @var array
     */
    protected static $partners = [];

    /**
     * Constructor
     * @param FileLocator $fileLocator
     */
    public function __construct(FileLocator $fileLocator)
    {
        $this->fileLocator = $fileLocator;
    }

    /**
     * Инициализация
     * Чтение файла и создание массива строк
     */
    public function init()
    {
        $fileName = $this->fileLocator->locate("@AppBundle/Resources/texts/{$this->type}.txt");

        $file = fopen($fileName, 'r');

        while ($line = fgets($file)) {
            $this->lines[] = $line;
        }
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Генерация рандомной строки
     * @return mixed
     */
    public function generateText()
    {
        if (!$this->initialized) {
            $this->init();
        }

        // Получение рандомной, сырой строки
        $line = $this->lines[rand(0, count($this->lines)-1)];

        // Вытаскиваем варинаты подстановок
        preg_match_all("/\{[0-9a-zа-яё\ \|]+\}/ui", $line, $matches);

        do {
            // Выборка и установление рандомного значения для вариантов подстановки
            foreach($matches[0] as $match) {
                $words = explode('|', substr($match, 1, -1));
                $word = $words[rand(0, count($words)-1)];
                $line = str_replace($match, $word, $line);
                $line = preg_replace("/\n/", '', $line);
            }

            // Проверка, не использовалось ли ранее уже эта строка
            $isUnique = !array_search($line, $this->used);
        } while (false == $isUnique);

        $this->used[] = $line;

        return $line;
    }

    public function getRandomPartnerName()
    {
        if (!self::$partners) {
            $file = $this->type == ServicerService::TYPE_NEWS ? 'partners' : 'g_partners';
            $fileName = $this->fileLocator
                ->locate("@AppBundle/Resources/texts/{$file}.txt");

            $file = fopen($fileName, 'r');
            while ($line = fgets($file)) {
                self::$partners[] = rtrim($line, "\n");
            }
        }

        return self::$partners[rand(0, count(self::$partners)-1)];
    }
}