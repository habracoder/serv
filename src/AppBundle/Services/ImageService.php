<?php

namespace AppBundle\Services;

use Symfony\Component\Config\FileLocator;

/**
 * Class ImageService
 * @package AppBundle\Services
 */
class ImageService
{
    /**
     * @var FileLocator
     */
    protected $fileLocator;

    /**
     * Расположение рандомных картинок
     * @var
     */
    protected $imagePath;

    /**
     * @param FileLocator $fileLocator
     */
    public function __construct(FileLocator $fileLocator)
    {
        $this->fileLocator = $fileLocator;
        $this->imagePath = $fileLocator->locate("../../web/images/");
    }

    /**
     * Возвращение ссылки на сгенерированную картинку
     * @return string
     */
    public function getImage()
    {
        $sizes = $this->getSizes();

        return $this->resize($this->getRandomImage(), $sizes['width'], $sizes['height']);
    }

    /**
     * Получение пути к рандомной картинке
     * @return mixed
     */
    protected function getRandomImage()
    {
        $images = [];
        foreach (glob($this->imagePath . '*') as $filename) {
            if (!is_dir($filename)) {
                $images[] = '/' . basename($filename);
            }
        }

        return $images[rand(0, count($images) - 1)];
    }

    /**
     * Опредиление размеров картинки
     * @return array
     */
    protected function getSizes()
    {
        preg_match("/([0-9]+)x([0-9]+)/", $_SERVER['REQUEST_URI'], $arr);

        return [
            'width' => isset($arr[1]) ? $arr[1] : 200,
            'height' => isset($arr[2]) ? $arr[2] : 200,
        ];
    }

    /**
     * Ресайзинг картинки на ходу, и возврат пути к ней,
     * при повоторных условиях возврат ранее сгенереной картинки
     * @param $filename
     * @param $width
     * @param $height
     * @return string|null
     */
    protected function resize($filename, $width, $height)
    {
        $filename = trim($filename, '/');
        // Уходим если файл не поддаеться обработке
        if (!is_file($this->imagePath . $filename)) {
            return null;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        // Сохраняем имя оригинального файла
        $oldImage = $filename;
        // Генерим имя нового файла в директории cache
        $newImage = 'cache/' . substr($filename, 0, strrpos($filename, '.'))
            . '-' . $width . 'x' . $height . '.' . $extension;

        // Создаем файл только если он отсутствует или устарел по сравнению с новым )
        if (!is_file($this->imagePath . $newImage) ||
            (filectime($this->imagePath . $oldImage) > filectime($this->imagePath . $newImage))) {
            $path = '';

            // Воссоздаем структуру директорий
            $directories = explode('/', dirname(str_replace('../', '', $newImage)));
            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;
                if (!is_dir($this->imagePath . $path)) {
                    @mkdir($this->imagePath . $path, 0777);
                }
            }
            list($width_orig, $height_orig) = getimagesize($this->imagePath . $oldImage);

            // Последний случай, если размеры совпадат с оригиналом то просто копируем.
            if ($width_orig != $width || $height_orig != $height) {
                $image = new Image($this->imagePath . $oldImage);
                $image->resize($width, $height);
                $image->save($this->imagePath . $newImage);
            } else {
                copy($this->imagePath . $oldImage, $this->imagePath . $newImage);
            }

            @chmod($this->imagePath . $newImage, 0777);
        }

        return $this->imagePath . $newImage;
    }
}