<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

$isDev = (bool) preg_match("/^dev\./", $_SERVER['HTTP_HOST']);

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
if ($isDev) {
    Debug::enable();
}

require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel($isDev ? 'dev' : 'prod', $isDev);
$kernel->loadClassCache();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
