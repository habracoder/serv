# Servicer

This is ...

### Nginx config

```nginx
server {
    listen   80;
    root /var/www/servicer/web;
    index index.php index.html index.htm;
    server_name "~(dev.)?servicer\.net|[a-z]{2}-[a-z]{2}.[marketgid|tovarro|mgid|lentainform|adskeeper]+\.[net|com]+";
    location / { try_files $uri $uri/ @rewrites; }
    location @rewrites { rewrite ^ /index.php last; }
    location ~ \.php(\/|$) {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        include /etc/nginx/fastcgi_params;
        fastcgi_param   APPLICATION_ENV development;
    }
    location ~ \.jpg$ {
        error_page 404 /tmp/200x200.gif;
    }
}
```